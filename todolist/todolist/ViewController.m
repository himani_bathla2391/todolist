//
//  ViewController.m
//  todolist
//
//  Created by Clicklabs 104 on 9/30/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *data;
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIButton *button2;

@property (weak, nonatomic) IBOutlet UITextField *textfield;
@property (weak, nonatomic) IBOutlet UITableView *table;
@end

@implementation ViewController
@synthesize label;
@synthesize button;
@synthesize button2;
@synthesize table;
@synthesize textfield;
- (void)viewDidLoad {
    [super viewDidLoad];
    data=[[NSMutableArray alloc]init];
    [textfield setEnabled :NO];
    [button2 setEnabled:NO];
    textfield.hidden= YES;
    button2.hidden= YES;
    table.hidden=YES;

    
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)button:(id)sender {
    [textfield setEnabled :YES];
    [button2 setEnabled:YES];
    textfield.hidden= NO;
    button2.hidden= NO;
    
}
- (IBAction)button2:(id)sender {
    [data addObject:textfield.text];
    [table reloadData];
    table.hidden=NO;
    [self setEditing:YES animated:YES];

}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return data.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellReuse"];
    cell.textLabel.text = data[indexPath.row];
    cell.textLabel.textColor = [UIColor blueColor];
    return cell;
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCellEditingStyle result = UITableViewCellEditingStyleNone;
        if ([tableView isEqual:self.table]){
        result = UITableViewCellEditingStyleDelete;
    }
        return result;
  }

- (void) setEditing:(BOOL)editing animated:(BOOL)animated
  {
    [super setEditing:editing animated:animated];
    [self.table setEditing:editing animated:animated];
  }

- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
  {
        if (editingStyle == UITableViewCellEditingStyleDelete){
        [data removeObjectAtIndex:indexPath.row];
        [table reloadData];}
  }


- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
